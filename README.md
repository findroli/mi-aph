# Semestral project for MI-APH

You play as a survivor of apocalypse. You have cooked a good looking steak, but zombies are aware of it and want it for themselves! Don't let them.

## About:
* Top-down shooter
* A steak randomly spawns at the beginning
* Portals spawn at random location every 10 seconds
* Each portal spawns 3 zombies which are trying to get to the steak
* You goal is to protect the steak
* Your weapons are pistol and obstacle building

## Controls:
* Arrows - movement
* X - shoot
* C hold - select building location
* C release - build on selected location
* Z - sprint

## How to run
* `npm install`
* `npm start`
* `http://localhost:1234/game1.html`
