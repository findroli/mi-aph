import { PlacementGridMap } from "./PlacementGridMap";
import { DEF_PORTAL_SPAWN_RATE } from "./Constants";

/**
 *  Trochu maly model. Hlavne obsahuje GridMapu.
 */
export class GameModel {

    //dynamic
    zombiesKilled = 0
    portalsSpawned = 0
    gameOver = false

    //static
    portalSpawnRate = DEF_PORTAL_SPAWN_RATE

    placementMap: PlacementGridMap
    
    constructor() {
        this.placementMap = new PlacementGridMap(screen.height/20)
    }

}