import Component from "../ts/engine/Component";
import { MSG_COLLISION, TAG_BUILDING, TAG_ENEMY, MSG_ENEMY_DIED } from "./Constants";
import { GameComponent } from "./GameComponent";
import Msg from "../ts/engine/Msg";
import { CollisionInfo } from "./CollisionInfo";

/**
 *  Jednoducha trieda, ktora riesi kolize.
 */
export class CollisionResolver extends GameComponent {

    onInit() {
        super.onInit();
        this.subscribe(MSG_COLLISION);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_COLLISION) {
            this.handleCollision(msg);
        }
    }

    protected handleCollision(msg: Msg) {
        let trigger = <CollisionInfo>msg.data;

        if (trigger.unit.getTag() == TAG_BUILDING) {
            trigger.projectile.remove()
        } else if (trigger.unit.getTag() == TAG_ENEMY) {
            trigger.projectile.remove()
            trigger.unit.remove()
            this.sendMessage(MSG_ENEMY_DIED)
        }
    }
}