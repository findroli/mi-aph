import Component from "../ts/engine/Component";
import { Direction } from "./Direction";
import { KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, KeyInputComponent, KEY_X, KEY_I, KEY_Z, KEY_C } from "../ts/components/KeyInputComponent";
import { MSG_PLAYER_SHOT, SPRINT_SPEED, WALK_SPEED, PISTOL_RATE, PISTOL_POWER, MSG_GAME_OVER } from "./Constants";
import { GameComponent } from "./GameComponent";
import Vec2 from "../ts/utils/Vec2";
import { checkTime } from "./Utils";
import Msg from "../ts/engine/Msg";

/**
 *  Komponenta hracovej postavy.
 */
export class PlayerComponent extends GameComponent {

    canMove = true
    speed = WALK_SPEED

    lastShot = 0
    fireRate = PISTOL_RATE
    firePower = PISTOL_POWER

    isBuilding = false

    onInit() {
        super.onInit()
        this.subscribe(MSG_GAME_OVER)
    }

    move(direction: Direction, delta: number) {
        if(!this.canMove) {
            return
        } 

        let pixiObj = this.owner.getPixiObj()
        switch (direction) {
            case Direction.DOWN:
                pixiObj.position.y += this.speed * delta
                pixiObj.rotation = Math.PI * 0.5
                break
            case Direction.UP:
                pixiObj.position.y -= this.speed * delta
                pixiObj.rotation = Math.PI * (-0.5)
                break
            case Direction.LEFT:
                pixiObj.position.x -= this.speed * delta
                pixiObj.rotation = Math.PI
                break
            case Direction.RIGHT:
                pixiObj.position.x += this.speed * delta
                pixiObj.rotation = 0
                break      
        }
    }

    onMessage(msg: Msg) {
        if(msg.action == MSG_GAME_OVER) {
            this.canMove == false
        }
    }

    tryShoot(absolute: number) {
        if (checkTime(absolute, this.lastShot, this.fireRate)) {
            this.lastShot = absolute
            this.sendMessage(MSG_PLAYER_SHOT)
            let pixiObj = this.owner.getPixiObj()
            let direction = new Vec2(Math.cos(pixiObj.rotation), Math.sin(pixiObj.rotation))
            this.factory.createBullet(this.owner.getScene().stage, this.model, pixiObj.position.x, pixiObj.position.y, direction)
        }
    }

    /**
     *  Zacne stavat - pred hracom sa bude ukazovat barevny ctverec na miste postaveni.
     */
    startBuilding() {
        this.isBuilding = true
        let pos = this.getBuildPosition()
        this.factory.createColoredTile(this.scene.stage, this.model, pos.x, pos.y, this);
    }

    /**
     *  Konec - prekazka sa postavi.
     */
    stopBuilding() {
        this.isBuilding = false
        let position = this.getBuildPosition()
        this.createBuilding(position.x, position.y)
    }

    /**
     *  Pozice dalsiho bloku gridu pred hracem.
     */
    getBuildPosition() {
        let pixiObj = this.owner.getPixiObj()
        let rotation = pixiObj.rotation
        let position = new Vec2(pixiObj.position.x, pixiObj.position.y)

        let direction = new Vec2(Math.cos(rotation), Math.sin(rotation))
        return this.model.placementMap.getNextTilePosition(position, direction)
    }

    createBuilding(x: number, y: number) {
        this.factory.createBuilding(this.scene.stage, this.model, x, y)
    }

}

/**
 *  Ovladaci komponenta hraca.
 */
export class PlayerInputComponent extends PlayerComponent {

    onUpdate(delta: number, absolute: number) {
        let cmp = this.scene.stage.findComponentByClass(KeyInputComponent.name)
        let cmpKey = <KeyInputComponent><any>cmp

        if(cmpKey.isKeyPressed(KEY_Z)) {
            this.speed = SPRINT_SPEED
        } else {
            this.speed = WALK_SPEED
        }

        if (cmpKey.isKeyPressed(KEY_LEFT)) {
            this.move(Direction.LEFT, delta)
        }
        if (cmpKey.isKeyPressed(KEY_RIGHT)) {
            this.move(Direction.RIGHT, delta)
        }
        if (cmpKey.isKeyPressed(KEY_UP)) {
            this.move(Direction.UP, delta)
        }
        if(cmpKey.isKeyPressed(KEY_DOWN)) {
            this.move(Direction.DOWN, delta)
        }

        if(cmpKey.isKeyPressed(KEY_X)) {
            this.tryShoot(absolute)
        }

        if(cmpKey.isKeyPressed(KEY_C)) {
            if(!this.isBuilding) {
                this.startBuilding()
            }
        }
        else if(this.isBuilding) {
            this.stopBuilding()
        }
    }

}