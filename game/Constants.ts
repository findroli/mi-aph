//player
export const SPRINT_SPEED = 0.15
export const WALK_SPEED = 0.1
export const PISTOL_RATE = 500
export const PISTOL_POWER = 20

//flags
export const FLAG_PROJECTILE = 1
export const FLAG_COLLIDABLE = 2

//tags
export const TAG_PLAYER = "player"
export const TAG_BUILDING = "building"
export const TAG_PORTAL = "portal"
export const TAG_ENEMY = "enemy"
export const TAG_MEAT = "meat"
export const TAG_BULLET = "bullet"
export const TAG_GAMEOVER = "game-over"

//textures
export const TEXTURE_PLAYER = "texture-player"
export const TEXTURE_BUILDING = "texture-building"
export const TEXTURE_PORTAL = "texture-portal"
export const TEXTURE_ENEMY = "texture-enemy"
export const TEXTURE_MEAT = "texture-meat"
export const TEXTURE_BULLET = "texture-bullet"

//sounds
export const SOUND_GUNSHOT = "sound-gunshot"

//messages
export const MSG_PLAYER_SHOT = "message-player-shot"
export const MSG_BUILDING_ADDED = "message-building-added"
export const MSG_BUILDING_REMOVED = "message-building-removed"
export const MSG_COLLISION = "message-collision"
export const MSG_MEAT_EATEN = "message-meat-eaten"
export const MSG_GAME_OVER = "message-game-over"
export const MSG_PORTAL_SPAWNED = "message-portal-spawned"
export const MSG_ENEMY_DIED = "message-enemy-died"

//other
export const DEF_BULLET_SPEED = 600
export const DEF_PORTAL_SPAWN_RATE = 10000
export const DEF_ZOMBIE_SPAWN_RATE = 2000
export const DEF_NUM_ZOMBIES_BY_PORTAL = 3
