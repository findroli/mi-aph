import Scene from "../ts/engine/Scene";
import { GameModel } from "./GameModel";
import PIXIObjectBuilder from "../ts/engine/PIXIObjectBuilder";
import { PIXICmp } from "../ts/engine/PIXIObject";
import { GameManager } from "./GameManager";
import { TEXTURE_PLAYER, TAG_PLAYER, TAG_BUILDING, TEXTURE_BUILDING, TAG_PORTAL, TEXTURE_PORTAL, TAG_ENEMY, TEXTURE_ENEMY, TAG_MEAT, TEXTURE_MEAT, TAG_BULLET, TEXTURE_BULLET, FLAG_COLLIDABLE, FLAG_PROJECTILE, DEF_BULLET_SPEED } from "./Constants";
import { PlayerComponent, PlayerInputComponent } from "./PlayerComponent";
import { KeyInputComponent } from "../ts/components/KeyInputComponent";
import { SoundComponent } from "./SoundComponent";
import DebugComponent from "../ts/components/DebugComponent";
import { BuildingComponent } from "./BuildingComponent";
import Vec2 from "../ts/utils/Vec2";
import { BuildTileComponent } from "./BuildTileComponent";
import { PortalComponent } from "./PortalComponent";
import { SpawnerComponent } from "./SpawnerComponent";
import { EnemyComponent } from "./EnemyComponent";
import { EnemyMoveComponent } from "./EnemyMoveComponent";
import { ATTR_DYNAMICS } from "../ts/engine/Constants";
import Dynamics from "../ts/utils/Dynamics";
import { ProjectileComponent } from "./ProjectileComponent";
import { CollisionManager } from "./CollisionManager";
import { CollisionResolver } from "./CollisionResolver";

/**
 *  Klasicka factory, kde su metody na inicializaciu hry a tvorenie hernych objektov.
 */
export class GameFactory {

    initGame(rootObject: PIXICmp.ComponentObject, model: GameModel) {
        let builder = new PIXIObjectBuilder(rootObject.getScene());
        var canvas = document.getElementById('gameCanvas') as HTMLCanvasElement;

        let background = new PIXICmp.Graphics()
        background.beginFill(0x94D211)
        background.drawRect(0, 0, canvas.width, canvas.height)
        background.position.set(0, 0)
        background.endFill()
        rootObject.getPixiObj().addChild(background)

        builder
            .withComponent(new GameManager(this, model))
            .withComponent(new KeyInputComponent())
            .withComponent(new SoundComponent())
            .withComponent(new SpawnerComponent(this, model))
            .withComponent(new CollisionManager())
            .withComponent(new CollisionResolver(this, model))
            .withComponent(new DebugComponent(document.getElementById("debugSect")))
            .build(rootObject)

        let player = new PIXICmp.Sprite(TAG_PLAYER, PIXI.Texture.fromImage(TEXTURE_PLAYER))
        let scale = model.placementMap.tileSize / player.width
        builder
            .relativePos(0.5, 0.5)
            .scale(scale)
            .anchor(0.5, 0.5)
            .withComponent(new PlayerInputComponent(this, model))
            .build(player, rootObject)
    }

    createBuilding(owner: PIXICmp.ComponentObject, model: GameModel, x: number, y: number) {
        let builder = new PIXIObjectBuilder(owner.getScene())

        let building = new PIXICmp.Sprite(TAG_BUILDING, PIXI.Texture.fromImage(TEXTURE_BUILDING))
        let scale = model.placementMap.tileSize / building.width
        builder
            .anchor(0.5, 0.5)
            .globalPos(x, y)
            .scale(scale)
            .withFlag(FLAG_COLLIDABLE)
            .withComponent(new BuildingComponent(this, model))
            .build(building, owner)

    }

    // Ctverec, ktory vyznacuje build position pre prekazku
    createColoredTile(owner: PIXICmp.ComponentObject, model: GameModel, x: number, y: number, player: PlayerComponent) {
        let builder = new PIXIObjectBuilder(owner.getScene())
        let tileSize = model.placementMap.tileSize

        let colorTile = new PIXICmp.Graphics()
        colorTile.beginFill(0x00FE2E)
        colorTile.drawRect(-tileSize/2, -tileSize/2, tileSize, tileSize)
        colorTile.endFill()

        builder
            .relativePos(x, y)
            .withComponent(new BuildTileComponent(this, model, player))
            .build(colorTile, owner)
    }

    createPortal(owner: PIXICmp.ComponentObject, model: GameModel, x: number, y: number) {
        let builder = new PIXIObjectBuilder(owner.getScene())
        let tileSize = model.placementMap.tileSize
        let texture = PIXI.Texture.fromImage(TEXTURE_PORTAL)
        texture.frame = new PIXI.Rectangle(0, 0, 100, 100)

        let portal = new PIXICmp.Sprite(TAG_PORTAL, texture)
        let scale = model.placementMap.tileSize / portal.width
        builder
            .anchor(0.5, 0.5)
            .globalPos(x, y)
            .scale(scale)
            .withComponent(new PortalComponent(this, model))
            .build(portal, owner)
    }

    createZombie(owner: PIXICmp.ComponentObject, model: GameModel, x: number, y: number) {
        let builder = new PIXIObjectBuilder(owner.getScene())

        let enemy = new PIXICmp.Sprite(TAG_ENEMY, PIXI.Texture.fromImage(TEXTURE_ENEMY))
        let scale = model.placementMap.tileSize / enemy.width
        let moveComp = new EnemyMoveComponent(this, model)
        builder
            .anchor(0.5, 0.5)
            .globalPos(x, y)
            .scale(scale)
            .withFlag(FLAG_COLLIDABLE)
            .withComponent(new EnemyComponent(this, model, moveComp))
            .withComponent(moveComp)
            .build(enemy, owner)
    }

    createMeat(owner: PIXICmp.ComponentObject, model: GameModel, x: number, y: number) {
        let builder = new PIXIObjectBuilder(owner.getScene())

        let meat = new PIXICmp.Sprite(TAG_MEAT, PIXI.Texture.fromImage(TEXTURE_MEAT))
        let scale = model.placementMap.tileSize / meat.width / 2
        builder
            .anchor(0.5, 0.5)
            .globalPos(x, y)
            .scale(scale)
            .build(meat, owner)
    }

    createBullet(owner: PIXICmp.ComponentObject, model: GameModel, x: number, y: number, direction: Vec2) {
        let builder = new PIXIObjectBuilder(owner.getScene())

        let dynamics = new Dynamics();
        dynamics.velocity = direction.multiply(DEF_BULLET_SPEED);
        let bullet = new PIXICmp.Sprite(TAG_BULLET, PIXI.Texture.fromImage(TEXTURE_BULLET))
        let scale = model.placementMap.tileSize / bullet.width / 10
        builder
            .scale(scale)
            .anchor(0.5, 0.5)
            .globalPos(x, y)
            .withFlag(FLAG_PROJECTILE)
            .withAttribute(ATTR_DYNAMICS, dynamics)
            .withComponent(new ProjectileComponent())
            .build(bullet, owner)
    }

    resetGame(scene: Scene) {
        scene.clearScene()
        
        let model = new GameModel()

        this.initGame(scene.stage, model)
    }

}