import Component from "../ts/engine/Component";
import { GameComponent } from "./GameComponent";
import Vec2 from "../ts/utils/Vec2";
import { MSG_BUILDING_ADDED, MSG_BUILDING_REMOVED } from "./Constants";

/**
 *  Trieda pre prekazku. Pri vytvoreni/zniceni sa prida/odoberie z GridMapy a posle dane spravy.
 */
export class BuildingComponent extends GameComponent {

    onInit() {
        let pos = this.owner.getPixiObj().position
        let obstructionPos = this.model.placementMap.getTileXY(new Vec2(pos.x, pos.y))
        this.model.placementMap.gridMap.addObstruction(obstructionPos)
        this.sendMessage(MSG_BUILDING_ADDED)
    }

    onRemove() {
        let pos = this.owner.getPixiObj().position
        let obstructionPos = this.model.placementMap.getTileXY(new Vec2(pos.x, pos.y))
        this.model.placementMap.gridMap.removeObstruction(obstructionPos)
        this.sendMessage(MSG_BUILDING_REMOVED)
    }

}