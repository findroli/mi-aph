import { GameComponent } from "./GameComponent";
import Vec2 from "../ts/utils/Vec2";
import { checkTime } from "./Utils";
import { ticker } from "pixi.js";
import { MSG_PORTAL_SPAWNED } from "./Constants";

/**
 *  Komponenta, ktora spawnuje maso (na zacatku) a pak v nejakom intervale portaly v nahodnych pozicich.
 */
export class SpawnerComponent extends GameComponent {

    lastSpawn = -1

    onInit() {
        this.spawnMeat()
    }

    onUpdate(delta: number, absolute: number) {
        if(this.lastSpawn == -1) {
            this.lastSpawn = absolute
            return
        }
        if(checkTime(absolute, this.lastSpawn, this.model.portalSpawnRate)) {
            this.spawnPortal()
            this.lastSpawn = absolute
        }
    }

    spawnPortal() {
        let position = this.getRandomTileCenter()
        this.factory.createPortal(this.owner.getScene().stage, this.model, position.x, position.y)
        this.sendMessage(MSG_PORTAL_SPAWNED)
    }

    spawnMeat() {
        let position = this.getRandomTileCenter()       
        this.factory.createMeat(this.owner.getScene().stage, this.model, position.x, position.y)
    }

    /**
     *  Random pozice bloku. Opakovane generuje pozice az kym nenajde nejaku bez prekazky.
     */
    getRandomTileCenter() {
        let map = this.model.placementMap

        let position = new Vec2(0, 0)
        let okayPosition = false
        while(!okayPosition) {
            let rnd1 = Math.random()
            let rnd2 = Math.random()
            let x = rnd1 * (map.gridMap.width - 1)
            let y = rnd2 * (map.gridMap.height - 1)
            let blockPos = new Vec2(x + rnd1, y + rnd2)
            position = map.getTileWorldCenter(blockPos)
            if(!map.gridMap.hasObstruction(blockPos)) {
                okayPosition = true
            }
        }

        return position
    }

}