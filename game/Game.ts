import { PixiRunner } from "../ts/PixiRunner";
import { TEXTURE_PLAYER, SOUND_GUNSHOT, TEXTURE_BUILDING, TEXTURE_PORTAL, TEXTURE_ENEMY, TEXTURE_MEAT, TEXTURE_BULLET } from "./Constants";
import { GameFactory } from "./GameFactory";

class Game {
    engine: PixiRunner;

    constructor() {
        this.engine = new PixiRunner();

        this.engine.init(document.getElementById("gameCanvas") as HTMLCanvasElement, 1);

        PIXI.loader
            .reset()
            .add(TEXTURE_BULLET, 'static/game/bullet.png')
            .add(TEXTURE_MEAT, 'static/game/meat.png')
            .add(TEXTURE_ENEMY, 'static/game/zombie.png')
            .add(TEXTURE_PORTAL, 'static/game/portal-sheet.png')
            .add(TEXTURE_BUILDING, 'static/game/crate.png')
            .add(TEXTURE_PLAYER, 'static/game/survivor.png')
            .add(SOUND_GUNSHOT, 'static/game/gunshot.mp3')
            .load(() => this.onAssetsLoaded());
    }

    onAssetsLoaded() {
        let factory = new GameFactory();
        factory.resetGame(this.engine.scene);
    }

}

new Game();