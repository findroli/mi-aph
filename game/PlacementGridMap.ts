import { GridMap, MAP_TYPE_TILE } from "../ts/utils/GridMap";
import Vec2 from "../ts/utils/Vec2";
import { AStarSearch, PathFinderContext } from "../ts/utils/Pathfinding";

/**
 *  Vlastna trieda pre mapu, ktora obsahuje niektore pomocne metody.
 */
export class PlacementGridMap {

    astar: AStarSearch
    gridMap: GridMap
    tileSize: number

    constructor(tileSize: number) {
        this.tileSize = tileSize
        let canvas = document.getElementById("gameCanvas") as HTMLCanvasElement
        let width = canvas.width / tileSize
        let height = canvas.height / tileSize

        this.gridMap = new GridMap(MAP_TYPE_TILE, 10, width, height);
        this.astar = new AStarSearch()
    }

    /**
     * Najde dalsi block gridu z danej pozicie danym smerem.
     */
    getNextTilePosition(position: Vec2, direction: Vec2) {
        let nextTile = position.add(direction.multiply(this.tileSize * 2))
        return this.getTileWorldCenter(this.getTileXY(nextTile))
    }

    /**
     *  Zo globalnych suradnic spravi suradnice bloku v gridu.
     */
    getTileXY(worldPos: Vec2) {
        let x = Math.floor(worldPos.x)
        let y = Math.floor(worldPos.y)
        return new Vec2(Math.floor(x / this.tileSize) , Math.floor(y / this.tileSize))
    }

    /**
     *  Zo suradnic bloku v gride spravi globalne suradnice stredu bloku.
     */
    getTileWorldCenter(tileXY: Vec2) {
        let x = Math.floor(tileXY.x)
        let y = Math.floor(tileXY.y)
        return new Vec2(x * this.tileSize + this.tileSize/2, y * this.tileSize + this.tileSize/2) 
    }

    getTilesWorldCenters(input: Array<Vec2>): Array<Vec2> {
        let output = new Array<Vec2>();
        for (let vec of input) {
            output.push(this.getTileWorldCenter(vec));
        }
        return output;
    }

    /**
     *  Najdenie cesty (vstup aj vystup su globalne suradnice).
     */
    findPath(startWorldPos: Vec2, targetWorldPos: Vec2): Array<Vec2> {
        let startPos = this.getTileXY(startWorldPos)
        let targetPos = this.getTileXY(targetWorldPos)
        let outputCtx = new PathFinderContext()
        this.astar.search(this.gridMap, startPos, targetPos, outputCtx, this.gridMap.indexMapper)
        return this.getTilesWorldCenters(outputCtx.pathFound)
    }

}