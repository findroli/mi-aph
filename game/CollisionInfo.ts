import { PIXICmp } from "../ts/engine/PIXIObject";

/**
 *  Trieda pre informacie zasielane v sprave o zachyteni kolizie.
 */
export class CollisionInfo {
    unit: PIXICmp.ComponentObject;
    projectile: PIXICmp.ComponentObject;

    constructor(unit: PIXICmp.ComponentObject, projectile: PIXICmp.ComponentObject) {
        this.unit = unit;
        this.projectile = projectile;
    }
}