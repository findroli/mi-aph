import Component from "../ts/engine/Component";
import { MSG_PORTAL_SPAWNED, MSG_GAME_OVER, MSG_ENEMY_DIED, MSG_MEAT_EATEN, TAG_GAMEOVER } from "./Constants";
import { GameComponent } from "./GameComponent";
import Msg from "../ts/engine/Msg";
import { PIXICmp } from "../ts/engine/PIXIObject";

export class GameManager extends GameComponent {

    gameOverText: PIXICmp.Text 
     
    onInit() {
        this.subscribe(MSG_PORTAL_SPAWNED, MSG_ENEMY_DIED, MSG_MEAT_EATEN)
        this.gameOverText = new PIXICmp.Text(TAG_GAMEOVER, "GAME OVER")
        this.gameOverText.style = new PIXI.TextStyle({
            fill: "0xFFFFFF"
        })
        this.gameOverText.visible = false
        this.owner.getScene().stage.getPixiObj().addChild(this.gameOverText)
    }

    onMessage(msg: Msg) {
        if(msg.action == MSG_ENEMY_DIED) {
            this.model.zombiesKilled += 1
        }
        else if(msg.action == MSG_PORTAL_SPAWNED) {
            this.model.portalsSpawned += 1
        }
        else if(msg.action == MSG_MEAT_EATEN) {
            this.gameOver()
        }
    }

    gameOver() {
        this.gameOverText.visible = true
        this.model.gameOver = true

        this.sendMessage(MSG_GAME_OVER)

        this.scene.invokeWithDelay(5000, () => {
            this.factory.resetGame(this.scene)
        })
    }

}