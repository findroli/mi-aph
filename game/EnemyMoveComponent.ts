import Vec2 from "../ts/utils/Vec2";
import { WALK_SPEED, MSG_MEAT_EATEN } from "./Constants";
import Component from "../ts/engine/Component";
import { GameComponent } from "./GameComponent";

/**
 *  Pohyb enemy. Funkcia 'goToPoint' najde cestu na gride a podla toho nastavi 'path', ktoru potom enemy nasleduje.
 */
export class EnemyMoveComponent extends GameComponent {

    // Index pre path, ku ktoremu prave smeruje
    nextPos = 0
    pathFinished = false
    // Cesta k targetu
    path = new Array<Vec2>()
    // Vektor smeru, ktorym ma ist k dalsiemu targetu v path (aby nemusel pocitat v kazdom kroku)
    moveVec = new Vec2(0, 0)

    speed = WALK_SPEED
    isMoving = false

    onUpdate(delta: number, absolute: number) {
        if(!this.isMoving) {
            return
        }

        this.makeStep(delta)        

        if(this.pathFinished) {
            this.stopMoving()
            this.eatMeat()
        }
    }

    makeStep(delta: number) {
        let target = this.path[this.nextPos]

        this.owner.getPixiObj().position.x += this.moveVec.x * delta * WALK_SPEED
        this.owner.getPixiObj().position.y += this.moveVec.y * delta * WALK_SPEED

        if(this.isNear(target, delta)) {
            this.nextTarget()
        }
    }

    nextTarget() {
        if(this.nextPos + 1 > this.path.length - 1) {
            this.pathFinished = true
            return
        }
        else {
            this.nextPos += 1
            let pos = new Vec2(this.owner.getPixiObj().position.x, this.owner.getPixiObj().position.y)
            let target = this.path[this.nextPos]
            this.moveVec = target.subtract(pos).normalize()
            this.owner.getPixiObj().rotation = Math.atan2(this.moveVec.y, this.moveVec.x)
        }
    }

    // Nastavi cestu na targetPos
    goToPoint(startPos: Vec2, targetPos: Vec2) {
        this.isMoving = false
        this.path = this.model.placementMap.findPath(startPos, targetPos)
        this.resetFollow(startPos)
    }

    resetFollow(startPos: Vec2) {
        if(this.path.length == 0) {
            return
        }

        this.nextPos = 0
        this.pathFinished = false
        let pos = new Vec2(startPos.x, startPos.y)
        let target = this.path[this.nextPos]
        this.moveVec = target.subtract(pos).normalize()
        this.isMoving = true
    }

    stopMoving() {
        this.isMoving = false
    }

    eatMeat() {
        this.sendMessage(MSG_MEAT_EATEN)
        this.owner.remove()
    }

    isNear(target: Vec2, delta: number) {
        let pos = this.owner.getPixiObj().position
        if(Math.abs(pos.x - target.x) < WALK_SPEED * delta && Math.abs(pos.y - target.y) < WALK_SPEED * delta) {
            return true
        }
        return false
    }

}