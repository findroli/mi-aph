import { DynamicsComponent } from "../ts/components/DynamicsComponent";
import { MSG_OBJECT_ADDED } from "../ts/engine/Constants";

/**
 *  Komponenta projektilu, ktory hrac vystreli.
 */
export class ProjectileComponent extends DynamicsComponent {

    onInit() {
        super.onInit()
        this.sendMessage(MSG_OBJECT_ADDED)
    }

    onUpdate(delta, absolute) {
        super.onUpdate(delta, absolute);

        let pos = this.owner.getPixiObj().position;
        if (pos.x < 0 || pos.x > this.scene.app.screen.width || pos.y < 0 || pos.y > this.scene.app.screen.height) {
            this.owner.remove();
        }
    }

}