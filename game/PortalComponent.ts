import { GameComponent } from "./GameComponent";
import { DEF_NUM_ZOMBIES_BY_PORTAL, DEF_ZOMBIE_SPAWN_RATE, DEF_PORTAL_SPAWN_RATE } from "./Constants";
import { checkTime } from "./Utils";
import Vec2 from "../ts/utils/Vec2";
import { ticker } from "pixi.js";

/**
 *  Komponenta portalu, ktory spawnuje v intervale zombikov. Po istom pocte zombikov zemre.
 */
export class PortalComponent extends GameComponent {

    lastZombieSpawn = -1
    zombiesLeft = DEF_NUM_ZOMBIES_BY_PORTAL
    spawnRate = DEF_ZOMBIE_SPAWN_RATE

    onUpdate(delta: number, absolute: number) {
        if(this.model.gameOver) {
            return
        }
        if(this.lastZombieSpawn == -1) {
            // Cakanie na spawn prveho...
            this.lastZombieSpawn = absolute + DEF_PORTAL_SPAWN_RATE / 2
            return
        }
        if(this.zombiesLeft > 0 && checkTime(absolute, this.lastZombieSpawn, this.spawnRate)) {
            this.lastZombieSpawn = absolute
            this.zombiesLeft -= 1
            this.spawnZombie()
            if(this.zombiesLeft <= 0) {
                this.vanishSelf()
            }
        }
    }

    spawnZombie() {
        let pixiObj = this.owner.getPixiObj()
        this.factory.createZombie(this.owner.getScene().stage, this.model, pixiObj.position.x, pixiObj.position.y)
    }

    vanishSelf() {
        this.owner.remove()
    }

}