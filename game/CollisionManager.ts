import Component from "../ts/engine/Component";
import { PIXICmp } from "../ts/engine/PIXIObject";
import { MSG_OBJECT_ADDED, MSG_OBJECT_REMOVED } from "../ts/engine/Constants";
import Msg from "../ts/engine/Msg";
import { FLAG_PROJECTILE, FLAG_COLLIDABLE, MSG_COLLISION } from "./Constants";
import { CollisionInfo } from "./CollisionInfo";

/**
 *  Trieda, ktora detekuje kolize. Podobna aka bola na cviceni.
 */
export class CollisionManager extends Component {

    projectiles = new Array<PIXICmp.ComponentObject>()
    collidables = new Array<PIXICmp.ComponentObject>()

    onInit() {
        super.onInit()
        this.subscribe(MSG_OBJECT_ADDED, MSG_OBJECT_REMOVED)
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_OBJECT_ADDED || msg.action == MSG_OBJECT_REMOVED) {
            this.projectiles = this.scene.findAllObjectsByFlag(FLAG_PROJECTILE);
            this.collidables = this.scene.findAllObjectsByFlag(FLAG_COLLIDABLE);
        }
    }

    // Porovnavanie kazdy s kazdym
    onUpdate(delta: number, absolute: number) {
        let collisions = new Array<CollisionInfo>();

        for (let projectile of this.projectiles) {
            for (let unit of this.collidables) {
                let boundsA = projectile.getPixiObj().getBounds();
                let boundsB = unit.getPixiObj().getBounds();

                let intersectionX = this.testHorizIntersection(boundsA, boundsB);
                let intersectionY = this.testVertIntersection(boundsA, boundsB);

                if (intersectionX > 0 && intersectionY > 0) {
                    collisions.push(new CollisionInfo(unit, projectile));
                }
            }
        }

        for (let collision of collisions) {
            this.sendMessage(MSG_COLLISION, collision);
        }
    }

    private testHorizIntersection(boundsA: PIXI.Rectangle, boundsB: PIXI.Rectangle): number {
        return Math.min(boundsA.right, boundsB.right) - Math.max(boundsA.left, boundsB.left);
    }

    private testVertIntersection(boundsA: PIXI.Rectangle, boundsB: PIXI.Rectangle): number {
        return Math.min(boundsA.bottom, boundsB.bottom) - Math.max(boundsA.top, boundsB.top);
    }

}