import { GameComponent } from "./GameComponent";
import { PlayerComponent } from "./PlayerComponent";
import { PIXICmp } from "../ts/engine/PIXIObject";
import { GameFactory } from "./GameFactory";
import { GameModel } from "./GameModel";

/**
 *  Trieda pre farebny ctverec, ktory ukazuje, kde hrac postavi prekazku.
 */
export class BuildTileComponent extends GameComponent {

    player: PlayerComponent

    constructor(factory: GameFactory, model: GameModel, player: PlayerComponent) {
        super(factory, model)
        this.player = player
    }

    // Pri kazdom update si pyta od playera svoju poziciu
    onUpdate(delta: number, absolute: number) {
        if(this.player.isBuilding) {
            let pos = this.player.getBuildPosition()
            this.owner.getPixiObj().position.set(pos.x, pos.y)
        }
        else {
            this.owner.remove()
        }
    }

}