import Component from "../ts/engine/Component";
import { GameFactory } from "./GameFactory";
import { GameModel } from "./GameModel";

/**
 *  Komponenta s odkazmi na factory a model.
 */
export class GameComponent extends Component {

    factory: GameFactory
    model: GameModel

    constructor(factory: GameFactory, model: GameModel) {
        super()
        this.factory = factory
        this.model = model
    }

}