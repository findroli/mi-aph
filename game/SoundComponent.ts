import { SOUND_GUNSHOT, MSG_PLAYER_SHOT } from "./Constants";
import { GenericComponent } from "../ts/components/GenericComponent";

/**
 *  Komponenta zvuku.
 */
export class SoundComponent extends GenericComponent {

    constructor() {
        super(SoundComponent.name);

        this.doOnMessage(MSG_PLAYER_SHOT, (cmp, msg) => this.playSound(SOUND_GUNSHOT));
    }

    playSound(soundName: string) {
        (<any>PIXI.loader.resources[soundName]).sound.play();
    }

}