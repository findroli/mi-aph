export function checkTime(time: number, lastTime: number, interval: number) {
    return time - lastTime > interval
}
