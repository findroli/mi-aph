import { WALK_SPEED, MSG_BUILDING_ADDED, MSG_BUILDING_REMOVED, TAG_MEAT } from "./Constants";
import { GameComponent } from "./GameComponent";
import Msg from "../ts/engine/Msg";
import { EnemyMoveComponent } from "./EnemyMoveComponent";
import { GameFactory } from "./GameFactory";
import { GameModel } from "./GameModel";
import Vec2 from "../ts/utils/Vec2";
import { MSG_OBJECT_ADDED } from "../ts/engine/Constants";

/**
 *  Trieda nepriatela. Ma odkaz na komponentu pohybu, ktoru vola ked treba zmenit trasu.
 */
export class EnemyComponent extends GameComponent {

    moveComponent: EnemyMoveComponent

    constructor(factory: GameFactory, model: GameModel, moveComp: EnemyMoveComponent) {
        super(factory, model)
        this.moveComponent = moveComp
    }

    onInit() {
        super.onInit()
        this.subscribe(MSG_BUILDING_ADDED, MSG_BUILDING_REMOVED)
        this.goToMeat()
        this.sendMessage(MSG_OBJECT_ADDED)
    }

    onMessage(msg: Msg) {
        if(msg.action != MSG_BUILDING_ADDED && msg.action != MSG_BUILDING_REMOVED) {
            return
        }

        this.goToMeat()
    }

    goToMeat() {
        let meat = this.owner.getScene().findFirstObjectByTag(TAG_MEAT)
        if(meat == null) {
            this.moveComponent.stopMoving()
            return
        }

        let meatPos = meat.getPixiObj().position
        let startPos = this.owner.getPixiObj().position
        this.moveComponent.goToPoint(new Vec2(startPos.x, startPos.y), new Vec2(meatPos.x, meatPos.y))
    }

}